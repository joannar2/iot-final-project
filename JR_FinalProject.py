import pygame
from pygame import mixer
import RPi.GPIO as GPIO
import time
import random
import os
from twilio.rest import Client

##########################
###   Initial Setups   ###
##########################

#Setting up GPIO pins
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

ledPin = 17
buttonPin = 12
workLedPin = 18
rgbPins = {'Red':13, 'Green':19, 'Blue':26}

GPIO.setup(buttonPin, GPIO.IN)
GPIO.setup(ledPin, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(workLedPin, GPIO.OUT, initial=GPIO.LOW)
for i in rgbPins:
    GPIO.setup(rgbPins[i], GPIO.OUT, initial=GPIO.LOW)

p_workLed = GPIO.PWM(workLedPin, 2000)
p_R = GPIO.PWM(rgbPins['Red'], 2000)
p_G = GPIO.PWM(rgbPins['Green'], 2000)
p_B = GPIO.PWM(rgbPins['Blue'], 2000)
p_workLed.start(0)
p_R.start(0)
p_G.start(0)
p_B.start(0)

workLed_step = 2 
workLed_delay = 0.05

#Initializing some variables
led_status = False
wl_status = 'life'
COLOR = [0xFF0000, 0x00FF00, 0x0000FF, 0xFFFF00, 0xFF00FF, 0x00FFFF]
workPlaylist = []
lifePlaylist = []
kill_time = 60 #time in seconds to run LifeMode before shutting down
text_sent = 0

# Starting the mixer
pygame.init()
mixer.init()
mixer.music.set_volume(0.5)

#Setting up text messaging
TWILIO_ACCOUNT_SID = 'AC2a66b63e6c92bb8a0303d7c32a8337a3' 
TWILIO_AUTH_TOKEN = '21287b05a1bbc470558e92d30b5c2a11' 
TWILIO_PHONE_SENDER = "6318258701"
TWILIO_PHONE_RECIPIENT = "6313554246"

##########################
###      Functions     ###
##########################

def buttonPress(ev=None):
    #Getting started...
    print('button pressed!')
    global led_status
    global wl_status    
    
    #Flip our status variables:
    led_status = not led_status
    if wl_status == 'work':
        wl_status = 'life'
    elif wl_status == 'life':
        wl_status = 'work'
    
    #Light up the LED embedded in the button:
    GPIO.output(ledPin, led_status)
    
    #Initiate work/life mode:
    if wl_status == 'work':
        workMode()
    elif wl_status == 'life':
        lifeMode()
        
def workMode():
    #Getting started...
    print("Work Mode Activated")
    global workLed_step
    global workLed_delay
    
    #Turn on the green LED light (status indicator)
    p_workLed.start(0)
    GPIO.output(workLedPin, GPIO.HIGH)
    
    #Ensure we kill LEDs from life mode:
    p_R.stop()
    p_G.stop()
    p_B.stop()
    for i in rgbPins:
        GPIO.output(rgbPins[i], GPIO.LOW)
    
    #Get music started and queue up the next song in our playlist:
    os.chdir('/home/pi/FinalProject/workPlaylist') 
    
    mixer.music.set_volume(0.5) #lower volume if not already
    mixer.music.load(workPlaylist.pop())
    mixer.music.queue(workPlaylist.pop())
    mixer.music.set_endevent(pygame.USEREVENT)
    mixer.music.play()
    
    #Continue to run through playlist until button is pressed again:
    time.sleep(.2)
    while GPIO.input(buttonPin) == False:
        
        #Requires multiple embedded checks for button press so we don't miss it while iterating through LED function
        for dc in range(0, 101, workLed_step):           
            if GPIO.input(buttonPin) == True:
                break          
            p_workLed.ChangeDutyCycle(dc)
            time.sleep(workLed_delay)
            
        if GPIO.input(buttonPin) == True:
            break
        time.sleep(0.5)

        for dc in range(100, -1, -workLed_step):
            if GPIO.input(buttonPin) == True:
                break
            p_workLed.ChangeDutyCycle(dc)
            time.sleep(workLed_delay)
            
        if GPIO.input(buttonPin) == True:
            break
        time.sleep(0.5)

        for event in pygame.event.get():
            if event.type == pygame.USEREVENT:
                if len(workPlaylist) > 0:
                    mixer.music.queue(workPlaylist.pop())
            if not mixer.music.get_busy():
                print('work playlist completed')
                break
            
def lifeMode():
    #Getting started...
    print("Life Mode Activated")
    global kill_time
    global text_sent
    start_time = time.time()
    
    #Ensure we kill LEDs from work mode:
    p_workLed.stop()
    GPIO.output(workLedPin, GPIO.LOW)
    for i in rgbPins:
        GPIO.output(rgbPins[i], GPIO.HIGH)
    
    #Get music started and queue up the next song in our playlist:
    os.chdir('/home/pi/FinalProject/lifePlaylist')    

    mixer.music.set_volume(1) #pump the jams
    mixer.music.load(lifePlaylist.pop())
    mixer.music.queue(lifePlaylist.pop())
    mixer.music.set_endevent(pygame.USEREVENT)
    mixer.music.play()
    
    #Start the disco ball LEDs:
    p_R.start(0)
    p_G.start(0)
    p_B.start(0)
    
    #Continue to light disco ball and run through playlist until button is pressed again:
    time.sleep(.2)
    while GPIO.input(buttonPin) == False:

        for color in COLOR:
            if GPIO.input(buttonPin) == True:
                break
            setColor(color)
            time.sleep(0.5)
            
        if GPIO.input(buttonPin) == True:
                break
        
        for event in pygame.event.get():
            if event.type == pygame.USEREVENT:
                if len(lifePlaylist) > 0:
                    mixer.music.queue(lifePlaylist.pop())
            if not mixer.music.get_busy():
                print('life playlist completed')
                break
            
        #This takes a second to send, so embedded here so lights and music are already going
        if text_sent == 0:
            print('sending text...')
            send_text_alert("WORK IS OVER!!!")
            text_sent += 1
    
        #Shutdown the pi if lifeMode has been running continuously for over 10 minutes:
        if time.time() > (start_time + kill_time):
            print('kaboom')
            time.sleep(15)
            destroy()
            os.system("sudo shutdown -h now")
            break
            
def MAP(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

def setColor(color):
                  
    R_val = (color & 0xFF0000) >> 16
    G_val = (color & 0x00FF00) >> 8
    B_val = (color & 0x0000FF) >> 0
    
    R_val = MAP(R_val, 0, 255, 0, 100)
    G_val = MAP(G_val, 0, 255, 0, 100)
    B_val = MAP(B_val, 0, 255, 0, 100)

    p_R.ChangeDutyCycle(R_val)
    p_G.ChangeDutyCycle(G_val)
    p_B.ChangeDutyCycle(B_val)
    
    time.sleep(0.3)
    
def createPlaylists(workPlaylist,lifePlaylist):
    
    for filename in os.listdir('/home/pi/FinalProject/workPlaylist'):
        workPlaylist.append(filename)
    for filename in os.listdir('/home/pi/FinalProject/lifePlaylist'):
        lifePlaylist.append(filename)
    
    #Option to shuffle order of songs:
    #random.shuffle(workPlaylist)
    #random.shuffle(lifePlaylist)
        
    #Option to sort songs alphabetically:
    workPlaylist = sorted(workPlaylist,reverse=True)
    lifePlaylist = sorted(lifePlaylist,reverse=True)
        
    
def send_text_alert(alert_str):
    """Sends an SMS text alert."""
    client = Client(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)
    message = client.messages.create(
        to=TWILIO_PHONE_RECIPIENT,
        from_=TWILIO_PHONE_SENDER,
        body=alert_str)
    print('Text Message Sent!')  
               
def destroy():
    p_workLed.stop()
    p_R.stop()
    p_G.stop()
    p_B.stop()
    
    GPIO.output(ledPin, GPIO.LOW)
    GPIO.output(workLedPin, GPIO.LOW)
    for i in rgbPins:
        GPIO.output(rgbPins[i], GPIO.LOW)
        
    GPIO.cleanup()
    
##########################
###      Main Code     ###
##########################
    
createPlaylists(workPlaylist,lifePlaylist)    
GPIO.add_event_detect(buttonPin, GPIO.FALLING, callback=buttonPress, bouncetime=200)

while True:
    
    print("Press 'p' to pause music, 'r' to resume music")
    print("Press 'e' to exit the program")
    query = input("  ")
    
    if query == 'w':
        workMode()      
    elif query == 'l':
        lifeMode()
    elif query == 'p':
        mixer.music.pause()     
    elif query == 'r':
        mixer.music.unpause()
    elif query == 'e':
        mixer.music.stop()
        destroy()
        break
        
        

